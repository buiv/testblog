from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.post_list, name='post_list'),
    #take the primary key from post.pk from post_list.html. Eg: /post/5/
    url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
]